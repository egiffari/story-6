    const formatBook = (books) => {
    $("#books").append(
       '<thead class="thead-dark">' +
          '<tr>' +
            '<th scope="col">title</th>' +
            '<th scope="col">Book Cover</th>' +
            '<th scope="col">Author</th>' +
            '<th scope="col">Details</th>' +
          '</tr>' +
        '</thead>'
    );
    $('#books').append("<tbody>");
    for(i=0 ; i < books.items.length; i++){
            $('#books').append(`<tr id=row${i}></tr>`);
            $(`#row${i}`).append("<td><p>" + books.items[i].volumeInfo.title + "</p></td>");                    
            try{    
            $(`#row${i}`).append("<td><img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></td>");
            }
            catch{
                $(`#row${i}`).append("<td> No image </td>");                   
            }
            try{    
                $(`#row${i}`).append(` <td> ${books.items[i].volumeInfo.authors.map((obj) => {return `<li>${obj}</li>`}).join("")} </td> `)    
            }
            catch{
                $(`#row${i}`).append("<td><li>Unknown authors</li></td>");  
            }
            try{
                $(`#row${i}`).append("<td><a href='" + books.items[i].volumeInfo.infoLink + "'>Details</a></td>");  
            }
            catch{
                $(`#row${i}`).append("<td> No details </td>");  
            }
            
        }
    $('#books').append("</tbody>");
    }

$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=data structures and algorithms',
        success: function(books){
            console.log(books);
            $('#books').empty();
            formatBook(books)
        }
    })

    $('#button').on('click', function(){
        let key = $("#search").val();
        $('#books')[0].innerHTML = "<h3> Searching... </h3>"
    
        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(books){
                // console.log(books.items[0].searchInfo);
                console.log(books.items)
                $('#books').empty();
                formatBook(books)
    
            }
        })
    })

})

