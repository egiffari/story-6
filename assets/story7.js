$(document).ready(()=>{
    //$("#accordion").accordion({header: "h2", collapsible: true, active: false});
    let isDark = false;

     $(".title").siblings().hide()
    
    $(".title").on('click', function() {
        if(!$(this).siblings().hasClass("shown")){
            $(".title").siblings().slideUp()
            $(".title").siblings().removeClass("shown")
            if(isDark){
                $(".title").removeClass("title-shown-dark")
                $(".title").addClass("isDark")
            }
            else{
                $(".title").removeClass("isDark")
                $(".title").removeClass("title-shown-light")
            }
            $(this).siblings().slideDown()
            $(this).siblings().addClass("shown")
            if(isDark){
                $("h2").removeAttr("style");
                $(this).removeClass("isDark")
                $(this).addClass("title-shown-dark")
            }
            else{
                $("h2").removeAttr("style");
                $(this).addClass("title-shown-light")
            }
        }
        else{
            if(isDark){
                $(".title").addClass("isDark")
            }
            $(".title").siblings().removeClass("shown")
            $(".title").removeClass("title-shown-dark")
            $(".title").removeClass("title-shown-light")
            $(".title").siblings().slideUp()
       
        }
    })
    $(".toggle").on("click", function(){
        if(isDark){
            $(".toggle").removeClass("isDark")
            $(".title").removeClass("isDark")
            $("body").css({backgroundImage:"url(/static/img/xerneas.jpg)"})
            $("h1").css({color: "white"})
            $("h2").css({color:"deepskyblue", backgroundColor: "white"})
            $(".desc").css({color:"deepskyblue", backgroundColor: "white"})
            isDark = !isDark;
        }
        else{
            $(".toggle").addClass("isDark")
            $(".title").addClass("isDark")
            $("body").css({backgroundImage:"url(/static/img/yveltal.png"})
            $("h1").css({color:"red"})
            $("h2").css({color:"red", backgroundColor:"black"})
            $(".desc").css({color:"red", backgroundColor:"black"})
            isDark = !isDark;
        }

    })
})
