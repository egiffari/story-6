from django.urls import path, include
from .views import view_story8


app_name="Story8"
urlpatterns = [
    path('', view_story8, name="story8"),
]