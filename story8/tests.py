import time
from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from .views import view_story8

# Create your tests here.
class TestStory8(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="home/chrome_78_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_view_status(self):
        handler = resolve('/story8')
        self.assertEqual(handler.func, view_story8)

    def test_html(self):
        response = self.client.get('/story8')
        self.assertTemplateUsed(response, 'story8.html')

    def test_click_button(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story8')
        self.browser.get('http://story6egi.herokuapp.com/story8')
        query = self.browser.find_element_by_id("search")
        query.send_keys("Fundamentals of Physics")
        button = self.browser.find_element_by_id("button")
        button.click()
        time.sleep(10)
        self.assertIn("Fundamentals of Physics", self.browser.page_source)
