from django import forms
from django.forms import widgets

class StatusForm(forms.Form):
    status = forms.CharField(label = "status", widget = forms.Textarea(attrs={"class": "statusfrm", "rows":20, "cols":205}))