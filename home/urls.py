from django.urls import path, include
from .views import view_status

app_name="home"

urlpatterns = [
    path('', view_status, name="status"),
]

