from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
import datetime

def view_status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
          s = form.data['status']
        #   if(len(s)>300):
        #       s = s[0:300]
          s = s[0:300]
          status = Status(status = s, time = datetime.datetime.now())
          status.save()
          return redirect('/')
    else:
        form = StatusForm()
    status_objects = Status.objects.all().order_by("time").reverse()
    context = {'form': form, "status_objects": status_objects}
    return render(request, 'index.html', context)