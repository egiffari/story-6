from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import datetime

# Create your tests here.
class TestStatus(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="home/chrome_78_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    #functional test
    def test_halo(self):
        # self.browser = webdriver.Chrome("home/chrome_78_driver/chromedriver.exe")
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://story6egi.herokuapp.com')

        halo = self.browser.find_element_by_class_name('halo').text
        self.assertEquals(halo, "Halo, apa kabar?")
        # self.browser.close()

    def test_correct_title(self):
        # self.browser = webdriver.Chrome("home/chrome_78_driver/chromedriver.exe")
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://story6egi.herokuapp.com')

        self.assertEquals(self.browser.title, "Egi's Status")
        # self.browser.close()

    def test_dummy_submission(self):
        # self.browser = webdriver.Chrome("home/chrome_78_driver/chromedriver.exe")
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://story6egi.herokuapp.com')

        status_form = self.browser.find_element_by_id("id_status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("this is a test")
        submit.click()
        self.assertIn("this is a test", self.browser.page_source)
        # self.browser.close()

    def test_correct_time_submission(self):
        # self.browser = webdriver.Chrome("home/chrome_78_driver/chromedriver.exe")
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://story6egi.herokuapp.com')

        status_form = self.browser.find_element_by_id("id_status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("this is a test for time")
        submit.click()
        self.assertIn(datetime.datetime.now().strftime("%d %b %Y | %H:%M"), self.browser.page_source)
        # self.browser.close()
    
    def test_max_status_length(self):
        # self.browser = webdriver.Chrome("home/chrome_78_driver/chromedriver.exe")
        # self.browser.get('http://localhost:8000')
        self.browser.get('http://story6egi.herokuapp.com')

        status_form = self.browser.find_element_by_id("id_status")
        submit = self.browser.find_element_by_id("submit")
        status_form.send_keys("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890<-End")
        submit.click()
        self.assertIn("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", self.browser.page_source)
        # self.browser.close()
    
    def test_landing_page(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_page_not_found(self):
        response = self.client.get('/this-page-doesnt-exist')
        self.assertEquals(response.status_code, 404)
    
    def test_redirect_post(self):
        response = self.client.post('/', {
            'status': "ppw is killing me"
        })
        self.assertEquals(response.status_code, 302)

