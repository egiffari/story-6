from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.urls import resolve
from .views import view_story7

# Create your tests here.
class TestStory7(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options, executable_path="home/chrome_78_driver/chromedriver.exe")
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.close()

    def test_title(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://story6egi.herokuapp.com/story7')

        self.assertEquals(self.browser.title, "Egi's Story 7")
        # self.browser.close()


    def test_accordion_activity(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://story6egi.herokuapp.com/story7')

        activities = self.browser.find_element_by_id("activities")
        activities.click()
        self.assertIn( "1) Staff of Human Resources", self.browser.page_source)

    def test_accordion_organization(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://story6egi.herokuapp.com/story7')

        activities = self.browser.find_element_by_id("organizations")
        activities.click()
        self.assertIn( "1) HR DPM Fasilkom UI", self.browser.page_source)

    def test_accordion_achievement(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://story6egi.herokuapp.com/story7')

        activities = self.browser.find_element_by_id("achievements")
        activities.click()
        self.assertIn( "3) Killed a Furious Rajang", self.browser.page_source)
    
    def test_egi(self):
        # self.browser = webdriver.Chrome('home/chrome_78_driver/chromedriver.exe')
        # self.browser.get('http://localhost:8000/story7')
        self.browser.get('http://story6egi.herokuapp.com/story7')
        self.assertIn("Hi, I'm Egi", self.browser.page_source)

    #unittest
    def test_view_status(self):
        handler = resolve('/story7')
        self.assertEqual(handler.func, view_story7)
    def test_html(self):
        response = self.client.get('/story7')
        self.assertTemplateUsed(response, 'story7.html')
