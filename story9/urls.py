from django.urls import path, include
from .views import view_story9, view_login, view_logout

app_name="story9"
urlpatterns = [
    path('', view_story9, name="story9"),
    path('login/', view_login, name="login"),
    path('logout/', view_logout, name="logout")
]
